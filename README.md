# RareExchange   

A [SkunkWorks](http://skunkworks.sourceforge.net) script for Asheron's Call emulator server.

## Functionality
- Exchanges unwanted rares to Rare Exchanger NPC

## Installation
- Install Decal. [decaldev.com](https://www.decaldev.com)
- Download SkunkWorks for Asheron's Call. [SkunkWorks35-500.exe](https://sourceforge.net/projects/skunkworks/files/SkunkWorks/3.5/SkunkWorks35-500.exe/download)  
- Install SkunkWorks to `C:/Games/Skunkworks`. Some users experience problems running SkunkWorks in the default Program Files directory.
- Download updated SkunkWorks skapi.dll file. [SkunkWorks3.5.509.zip](https://sourceforge.net/projects/skunkworks/files/SkunkWorks/3.5/SkunkWorks3.5.509.zip/download)
- Extract SkunkWorks3.5.509.zip to your SkunkWorks directory, overwriting the existing skapi.dll file.
- Download [RareExchange](https://gitlab.com/Cyprias/rareexchange/-/releases) and extract it to your SkunkWorks directory. 
- Launch AC and run the `RareExchange.swx` file from the in game Skunkworks UI.


## Updating RareExchange
- Download a newer version from [here](https://gitlab.com/Cyprias/rareexchange/-/releases)  
- Open the zip and extract the files over your existing RareExchange folder, overwriting the previous files. Your settings will remain intact.  

## Infrequently Asked Questions
##### How does the UI work?
The list populates with gems you have in your inventory. Enabling the checkbox indicates you want to give that rare to the rare exchanger.  
The Run button will begin giving rares and MMDs to the rare exchanger until you've exausted all your rares or MMDs.  
The Refresh button will refresh the list if needed.

##### I'm getting a `>> Skunk***.js: File not found` error. What do I do?  
Simple answer: The zip file GitLab creates lack certain files. Goto the [releases](https://gitlab.com/Cyprias/RareExchange/-/releases) page and use the `Download: RareExchange_x.x.xxx.zip` link in the description.   
Technical answer: This repo uses [Git submodules](https://www.atlassian.com/git/tutorials/git-submodule) to share common files used in multiple SkunkWorks projects. GitLab doesn't include submodules files in their download link.  

##### I can't see Skunkworks' icon on the Decal bar.
Try installing the following libraries, then restart your PC.  
- [Microsoft .NET Framework 2.0 Service Pack 2](https://www.microsoft.com/en-us/download/details.aspx?id=1639)  
- [MSXML (Microsoft XML Parser) 3.0 Service Pack 4 SDK](https://www.microsoft.com/en-us/download/details.aspx?id=23412)  
- [MSXML 4.0 Service Pack 3 (Microsoft XML Core Services)](https://www.microsoft.com/en-us/download/details.aspx?id=15697)  
- [Microsoft Core XML Services (MSXML) 6.0](https://www.microsoft.com/en-us/download/details.aspx?id=3988)  

Also try running ThwargleLauncher without admin privileges.


##### Why does SkunkWorks reduce my frame rate so much?  
SkunkWorks uses Decal's old style render engine which isn't as efficient as the newer [Virindi Views](http://www.virindi.net/wiki/index.php/Virindi_Views) render engine that many plugins use today.  
Minimizing script UI should alleviate the issue.


## External links
- [SkunkWorks source repo](https://gitlab.com/Cyprias/SkunkWorks)  
- [SkunkWorks Discord channel](https://discord.gg/z5wXR5K)  

## Contributors
- [Cyprias](https://gitlab.com/cyprias)

## License
MIT License	(http://opensource.org/licenses/MIT)