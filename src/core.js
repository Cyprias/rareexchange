var MAJOR = "RareExchange-1.0";
var MINOR = 210728; // Year Month Day

(function (factory) {
	var params = {};
	params.EventEmitter   = (typeof EventEmitter !== "undefined" && EventEmitter) || require("modules\\SkunkSuite\\EventEmitter"); 
	
	// eslint-disable-next-line no-undef
	params.SkunkSchema11 = (typeof SkunkSchema11 !== "undefined" && SkunkSchema11);
	
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(params);
	} else {
		// eslint-disable-next-line no-undef
		RareExchange10 = factory(params); // Global object.
	}
}(function (params) {
	var EventEmitter = params.EventEmitter;
	var SkunkSchema11 = params.SkunkSchema11;
	
	var core = LibStub("SkunkScript-1.0").newScript(new EventEmitter(), 
		MAJOR, 
		"SkunkTimer-1.0", 
		"SkunkLogger-1.0", 
		"SkunkSchema-1.1", 
		"SkunkActions-1.0",
		"SkunkConfig-1.0");

	core.setShortName("RE");
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;
	
	core.version         = MAJOR + "." + MINOR; 
	core.title           = core.version;

	var ONE_SECOND = 1000;
	var ICON_START = 0x6000000;
	
	var defaultConfig = {
		profile: {
			exchangeRares: {}
		}
	};

	core.main = function main() {
		if (skapi.plig == pligNotRunning) {
			core.console("****************************************************");
			core.console("Asheron's call isn't running. Cannot launch script.");
			core.console("****************************************************");
			return console.StopScript();
		} else if (skapi.plig == pligAtLogin) {
			core.console("****************************************************");
			core.console("A character hasn't logged in yet. Cannot launch script.");
			core.console("****************************************************");
			return console.StopScript();
		}

		core.initialize();

		// SkunkWorks can launch scripts at the character script, so wait until you're in world before proceeding)
		while (skapi.plig != pligInWorld) {
			core.console("Waiting to finish entering world... (plig: " + skapi.plig + ")");
			skapi.WaitEvent(ONE_SECOND, wemFullTimeout);
		}

		core.enable();

		while (core._script.enabledState == true) {
			skapi.WaitEvent(100, wemFullTimeout);
		}

		// Tell SkunkWorks to remove all panels except its own.
		skapi.RemoveControls("!SkunkWorks");
		
		skapi.OutputLine("console.StopScript()", opmConsole);
		// eslint-disable-next-line no-console
		console.StopScript();
	};
	
	core.on("onInitialize", function onInitialize() {
		core.debug("<onInitialize>");
		
		core.skunkConfig = new core.Config("./configs", defaultConfig).load()
			.save();
		
		if (skapi.plig == pligInWorld) {
			core.showGUI();
		}
		
	});

	core.on("onEnable", function onEnable() {
		core.debug("<onEnable>");
		
	});

	core.isRare = function isRare(aco) {
		if (aco.szName.match(/^(.*)'s Crystal$/)) return true;
		if (aco.szName.match(/^(.*)'s Pearl$/)) return true;
		if (aco.szName.match(/^(.*)'s Jewel$/)) return true;
		if (aco.szName.match(/^Pearl of (.*)$/)) return true;
		return false;
	};

	core.showGUI = function showGUI() {
		var viewTitle = "Rare Exchange";
		var Element = SkunkSchema11.Element;
		var schema = Element("view", {title: viewTitle, icon: 1234, width: 200, height: 270}, [
			Element("control", {progid: 'DecalControls.FixedLayout'}, [
				Element("control", {progid: 'DecalControls.list', name: "lstRares", width: 200, height: 220}, [
					Element("column", {progid: "DecalControls.CheckColumn"}),
					Element("column", {progid: "DecalControls.IconColumn"}),
					Element("column", {progid: "DecalControls.TextColumn", fixedwidth: 160})
				]),
				
				Element("control", {progid: 'DecalControls.PushButton', name: "btnRun",      width: 40, height: 20, top: 220, left: 0,   text: "Run"}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnRefresh",  width: 50, height: 20, top: 220, left: 40,  text: "Refresh"}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnQuit",     width: 40, height: 20, top: 220, left: 160, text: "Quit"})
			])
		]);
		
		var rows;
		function populateList() {
			var col = 0;
			skapi.GetControlProperty(viewTitle, "lstRares", "Clear()");
			skapi.GetControlProperty(viewTitle, "lstRares", "AddRow()");
			skapi.SetControlProperty(viewTitle, "lstRares", "Data(" + 0 + ", " + col + ")", false);
			skapi.SetControlProperty(viewTitle, "lstRares", "Data(" + 1 + ", " + col + ", 1)", 0);
			skapi.SetControlProperty(viewTitle, "lstRares", "Data(" + 2 + ", " + col + ")", "Rare");
			
			var rares = core.getInventory().filter(core.isRare);
			
			var rareNames = {};
			rares.forEach(function(aco) {
				rareNames[aco.szName] = true;
			});
			
			rares = Object.keys(rareNames);
			
			//core.console("rares: " + rares);
			rares.sort(function(a, b) {
				if (a < b) return -1;
				if (a > b) return 1;
				return 0;
			});
			
			rows = [];
			rares.forEach(function(szName) {
				col++;
				var aco = skapi.AcoFromSz(szName);
				var exchangeRares = core.skunkConfig.profile.exchangeRares[aco.szName] || false;
				skapi.GetControlProperty(viewTitle, "lstRares", "AddRow()");
				skapi.SetControlProperty(viewTitle, "lstRares", "Data(" + 0 + ", " + col + ")", exchangeRares);
				skapi.SetControlProperty(viewTitle, "lstRares", "Data(" + 1 + ", " + col + ", 1)", ICON_START + aco.icon);
				skapi.SetControlProperty(viewTitle, "lstRares", "Data(" + 2 + ", " + col + ")", aco.szName);
				rows.push(aco);
			});
			
		}

		var handler = {};
		handler.OnControlEvent = function OnControlEvent(szPanel, szControl, dictSzValue) {
			if (szPanel != viewTitle) return;
			if (szControl == "btnQuit") {
				return core.disable();
			} else if (szControl == "lstRares") {
				var value = dictSzValue(szControl);
				var intCol = parseInt(value.split(",")[0], 10);
				var intRow = parseInt(value.split(",")[1], 10);
				var aco = rows[intRow - 1];
				if (intCol == 0) {
					return SkunkSchema11.getControlPropertyAsync({
						panelName  : viewTitle,
						controlName: "lstRares",
						property   : "data(" + intCol + "," + intRow + ")"
					}, function(err, results) {
						if (err) core.warn("Error getting list value", err);
						core.skunkConfig.profile.exchangeRares[aco.szName] = results;
						core.skunkConfig.saveProfile();
					});
				} else {
					return skapi.SelectAco(aco);
				}
			} else if (szControl == "btnRun") {
				skapi.SetControlProperty(viewTitle, szControl, "text", "...");
				return core.exchangeRares({logger: core.debug}, function(err) {
					skapi.SetControlProperty(viewTitle, szControl, "text", "Run");
					if (err) return core.warn("Error during run", err);
					core.info("Done");
					populateList();
				});
			} else if (szControl == "btnRefresh") {
				populateList();
			}
			
			core.console("<OnControlEvent>", szPanel, szControl, value);
		};
		skapi.AddHandler(evidOnControlEvent, handler);
		
		
		var string = schema.toXML();
		var pretty = SkunkSchema11.prettifyXml(string);
		core.console("pretty: " + pretty);
		skapi.ShowControls(string, true);
		populateList();
	};

	core.getInventory = function getInventory() {
		var items = [];
		var acoPack, aco;
		// eslint-disable-next-line no-restricted-syntax
		for (var pack = 0; pack < skapi.cpack; pack++) {
			acoPack = skapi.AcoFromIpackIitem(pack, iitemNil);
			// eslint-disable-next-line no-restricted-syntax
			for (var slot = 0; slot < acoPack.citemContents; slot++) {
				aco = skapi.AcoFromIpackIitem(pack, slot);
				items.push(aco);
			}
		}
		return items;
	};

	core.applyMethod = function applyMethod() {
		//core.trace("<applyMethod>");
		var args = [].slice.call(arguments);
		var thisArg;
		if (typeof args[0] !== "function") {
			thisArg = args.splice(0, 1)[0];
		}
		var method = args.splice(0, 1)[0];
		var methodArgs = args.splice(0, 1)[0];
		return method && method.apply(thisArg, methodArgs);
	};

	core.getMMDs = function getMMDs() {
		var MMDs = core.getInventory().filter(function(aco) {
			return aco.szName == "Trade Note (250,000)";
		});
		
		// Highest stacks first.
		MMDs.sort(function(a, b) {
			return b.citemStack - a.citemStack;
		});
		
		return MMDs;
	};

	core.exchangeRares = function exchangeRares(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		
		function finish() {
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var acoRareExchanger = skapi.AcoFromSz("Rare Exchanger");
		if (!acoRareExchanger || !acoRareExchanger.fExists) {
			core.warn("Can't find Rare Exchanger NPC");
			return finish(new core.Error("CANT_FIND_RARE_EXCHANGER"));
		}
		
		var rares, MMDs;
		return async_js.whilst(
			function test() { 
				rares = core.getInventory().filter(core.isRare)
					.filter(function(aco) {
						return core.skunkConfig.profile.exchangeRares[aco.szName] == true;
					});
				if (rares.length == 0) return false;
				MMDs = core.getMMDs();
				if (MMDs.length == 0) return false;
				var acoMMD = MMDs[0];
				if (acoMMD.citemStack < 2) return false;
				return true;
			},
			function iter(callback) {
				var acoRare = rares[0];
				var acoMMD = MMDs[0];
				logger("Giving " + acoMMD.szName + "...");
				return core.giveToAco({logger: logger, acoItem: acoMMD, acoTarget: acoRareExchanger}, onGiveMMD);
				function onGiveMMD(err) {
					if (err) return callback(err);

					//core.info("results: " + results);
					logger("Giving " + acoRare.szName + "...");
					return core.giveToAco({logger: logger, acoItem: acoRare, acoTarget: acoRareExchanger}, onGiveRare);
				}
				function onGiveRare(err) {
					if (err) return callback(err);
					core.stackInventory({logger: logger}, callback);
				}
			},
			finish
		);
	};

	core.stackInventory = function stackInventory(params, callback) {
		core.debug("<stackInventory>");

		//var verbose = (!params || params.verbose === undefined ? core.debug : params.verbose);
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		
		
		var acf = skapi.AcfNew();
		acf.olc = olcInventory;
		if (params && params.mcm) {
			acf.mcm = params.mcm;
		}
		var items = core.coacoToArray(acf.CoacoGet()).filter(function _canStack(aco) {
			return aco.citemMaxStack > 1 && aco.citemStack < aco.citemMaxStack;
		});

		items.sort(function(a, b) {
			return a.citemStack - b.citemStack;
		});
		
		return async_js.eachSeries(items, function _each(aco, callback) {
			//core.debug("<stackInventory|_each>");
			if (!aco || !aco.fExists) return core.setImmediate(callback);

			var partners = core.getItemStackPartners({aco: aco, logger: logger, max: aco.citemMaxStack - 1});
			if (partners.length == 0) return core.setImmediate(callback);
			logger("aco: " + aco.szName, aco.citemStack, "partners: " + partners.length);

			partners.sort(function(a, b) {
				return b.citemStack - a.citemStack;
			});

			var acoPartner = partners[0];
			
			logger(aco.szName + " (" + aco.citemStack + "/" + aco.citemMaxStack + ")'s stack partner is " + acoPartner.szName + " (" + acoPartner.citemStack + "/" + acoPartner.citemMaxStack + ")");
			if (!acoPartner || !acoPartner.fExists) return core.setImmediate(callback);

			core.mergeItems({
				aco       : aco,
				acoPartner: acoPartner,
				logger    : logger
			}, function(err, results) {
				if (err) {
					if (err.message == "MUST_PICKUP_FIRST") {
						core.warn(aco.szName + " isn't in our iventory, phantom?");
						core.phantomItems[aco.oid] = true;
						return callback(undefined, true);
					}
					return callback(err);
				}
				callback(undefined, results);
			});
		}, callback);
	};

	core.coacoToArray = function coacoToArray(coaco) {
		var arr = [];
		// eslint-disable-next-line no-restricted-syntax
		for (var i = 0; i < coaco.Count; i++) {
			arr.push(coaco.Item(i));
		}
		return arr;
	};

	core.getItemStackPartners = function getItemStackPartners(params) {
		//core.debug("<getItemStackPartner>");
		var aco = params.aco;
		var max = (!params || params.max === undefined ? aco.citemMaxStack : params.max);

		var acf = skapi.AcfNew();
		acf.olc = olcInventory;
		acf.szName = aco.szName;
		acf.citemStackMax = max;

		var items = core.coacoToArray(acf.CoacoGet());
		
		// Remove aco from available items.
		return items.filter(function(acoPartner) {
			return acoPartner.citemMaxStack > 1 && aco.icon == acoPartner.icon && acoPartner.oid != aco.oid;
		});
	};

	return core;
}));

if (typeof RareExchange10 !== "undefined") {
	// eslint-disable-next-line no-undef
	main = RareExchange10.main;
}